from typing import Union

from fastapi import FastAPI, status, Response
from pydantic import BaseModel
from hashlib import sha1
import redis

class UrlItem(BaseModel):
    url: str

app = FastAPI()

@app.post("/")
def shorten_url(urlItem: UrlItem):
    url = urlItem.url.encode("utf-8")
    r = redis.Redis(host='localhost', port=6379, decode_responses=True)
    key = sha1(url).hexdigest()[:6]

    if not r.exists(key):
        print("URL not in storage, creating key and storing")
        r.set(key, url)
    else:
        current_url = r.get(key).encode("utf-8")
        # Handle collision
        if current_url != url:
            unique_diff = random() * sys.maxint
            key = sha1(url + str(unique_diff)).hexdigest()[:6]
            r.set(key, url)

    short_url = f"http://localhost/{key}"

    return {"url": url, "short_url": short_url}

@app.get("/{url_key}", status_code=status.HTTP_302_FOUND)
def redirect_url(url_key: str, response: Response):
    # Check if we have this key
    r = redis.Redis(host='localhost', port=6379, decode_responses=True)
    url_key = url_key.encode("utf-8")
    if not r.exists(url_key):
        response.status_code = status.HTTP_404_NOT_FOUND
        return

    url = r.get(url_key)
    response.headers["Location"] = url

@app.delete("/{url_key}")
def delete_short(url_key: str, response: Response):
    r = redis.Redis(host='localhost', port=6379, decode_responses=True)
    url_key = url_key.encode("utf-8")
    if r.exists(url_key):
        r.delete(url_key)
    else:
        response.status_code = status.HTTP_404_NOT_FOUND
