PyURLs
======

A dead simple URL shortening backend service based on FastAPI and Redis.

To run locally, run a redis-server on the default port (6379) then in another
console, run

    pip install -r requirements.txt
    uvicorn main:app

The service is self-documenting through FastAPI, and the documentation can be
found at `http://localhost:8000/docs`.

It produces URLs with a 6-character key derived from a sha1 hash then
truncated.
